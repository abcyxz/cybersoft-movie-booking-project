import axios from "axios";
import { BASE_URL, TOKEN_CYBERSOFT } from '../constants/config';
const axiosClient = axios.create({
  baseURL: BASE_URL,
})
axiosClient.interceptors.request.use((config) => { //tất cả request đều phải qua đây 
  const user = localStorage.getItem('user');

  if (user) { // nếu có đăng nhập thì thực hiện
    const { accessToken } = JSON.parse(user)
    config.headers.Authorization = `Bearer ${accessToken}`;
  }
  config.headers.TokenCybersoft = TOKEN_CYBERSOFT; 
  return config;
})

export default axiosClient;
